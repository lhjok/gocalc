package main

import (
	"errors"
	"math"
	"os"
	"strconv"
	// 下面是第三方或自定义库
	"github.com/andlabs/ui"
	"github.com/lhjok/stack"
)

// 全局数字栈和运算符栈类型
var stackA = stack.NewStack()
var stackB = stack.NewStack()

const pi float64 = 3.141592653589793

////////////////////////////////////////////////////////////////////////////////////////////////////////////
func main() {
	// 原生GO语言跨平台UI库
	clean := true
	err := ui.Main(func() {
		name := ui.NewEntry()       // 创建一个文本框
		button := ui.NewButton("=") // 创建一个按钮
		box := ui.NewVerticalBox()
		box.Append(name, false)
		box.Append(button, false)
		box.SetPadded(true) // 开启组件间预留间隙
		window := ui.NewWindow("%=MOD  ^=POW  A=ABS  S=SQRT  P=PI", 500, 98, false)
		window.SetChild(box)
		window.SetMargined(true) // 开启窗口边框预留间隙
		button.OnClicked(func(*ui.Button) {
			// 点击按钮把表达式传入到混合四则运算函数
			if clean {
				expr := name.Text() + "="
				result, errors := run(expr)
				if errors != nil {
					clean = false
					name.SetText(errors.Error()) // 返回错误信息
				} else {
					name.SetText(strconv.FormatFloat(result, 'f', -1, 64)) // 输出计算结果
				}
			} else {
				clean = true
				name.SetText("") // 清除错误信息
			}
		})
		window.OnClosing(func(*ui.Window) bool {
			ui.Quit() // 退出程序
			return true
		})
		window.Show()
	})
	if err != nil {
		panic(err) // 程序发生了错误
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
func run(expr string) (float64, error) {
	var (
		ab     rune    // 数字栈和运算符标记
		co, bk uint32  // 切片定位标记和括号匹配标记
		sf     float64 // 数字进栈前变量
		errs   error   //字符串转数字时返回的错误类型
	)
	// 清空数字栈
	for stackA.Size() != 0 {
		stackA.Pop()
	}
	// 清空运算符栈
	for stackB.Size() != 0 {
		stackB.Pop()
	}
	// 迭代字符串表达式
	for i, v := range expr {
		switch v {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.':
			continue
		case '+', '-', '*', '/', '%', '^':
			if ab != 'A' {
				sf, errs = strconv.ParseFloat(expr[co:i], 64) // 将运算符前的数字取出来
				if errs != nil {
					return 0, errors.New("Expression Error")
				}
				stackA.Push(sf) // 读取的数字进栈
				ab = 'A'
			}
			for stackB.Size() != 0 && stackB.Top() != '(' { // 运算符栈为空或遇到左括号时停止循环
				Bo := stackB.Top()
				p1 := priority(Bo)
				p2 := priority(v)
				if p1 >= p2 { // 优先级比较
					Re, err := computing(Bo) // 调用双目运算函数以及内部数学库函数
					if err != nil {
						return 0, err
					}
					stackA.Push(Re) // 运算结果进栈
					stackB.Pop()    // 运算符出栈
				} else {
					break
				}
			}
			stackB.Push(v)     // 运算符进栈
			co = uint32(i) + 1 // 移动切片定位标记
			ab = 'B'
			continue
		case '(':
			if ab != 'A' {
				stackB.Push(v)     // 左括号直接进栈
				co = uint32(i) + 1 // 移动切片定位标记
				bk++
				continue
			}
			return 0, errors.New("Expression Error")
		case ')':
			if ab != 'A' {
				sf, errs = strconv.ParseFloat(expr[co:i], 64) // 将运算符前的数字取出来
				if errs != nil {
					return 0, errors.New("Expression Error")
				}
				stackA.Push(sf) // 读取的数字进栈
				ab = 'A'
			}
			if bk > 0 && ab == 'A' { // 运算符栈中必须有左括号与右括号前必须是数字
				for stackB.Top() != '(' { //遇到左括号时停止循环
					Bo := stackB.Pop()
					Re, err := computing(Bo) // 调用双目运算函数以及内部数学库函数
					if err != nil {
						return 0, err
					}
					stackA.Push(Re) // 运算结果进栈
				}
				stackB.Pop()       // 运算符出栈
				co = uint32(i) + 1 // 移动切片定位标记
				bk--
				continue
			}
			return 0, errors.New("Expression Error")
		case '=':
			if ab != 'A' {
				sf, errs = strconv.ParseFloat(expr[co:i], 64) // 将运算符前的数字取出来
				if errs != nil {
					return 0, errors.New("Expression Error")
				}
				stackA.Push(sf) // 读取的数字进栈
				ab = 'A'
			}
			if bk > 0 {
				return 0, errors.New("Expression Error")
			}
			for stackB.Size() != 0 { // 直到运算符栈为空停止循环
				Bo := stackB.Pop()
				Re, err := computing(Bo) // 调用双目运算函数以及内部数学库函数
				if err != nil {
					return 0, err
				}
				stackA.Push(Re) // 运算结果进栈
			}
			Z := stackA.Pop().(float64) // 清空最后一个数据栈
			return Z, nil
		case 'A':
			if ab != 'A' {
				sf, errs = strconv.ParseFloat(expr[co:i], 64) // 将运算符前的数字取出来
				if errs != nil {
					return 0, errors.New("Expression Error")
				}
				stackA.Push(sf) // 读取的数字进栈
				ab = 'A'
			}
			if ab == 'A' {
				Re := stackA.Pop().(float64)
				stackA.Push(math.Abs(Re)) // Abs(X) X的绝对值
				co = uint32(i) + 1        // 移动切片定位标记
				continue
			}
			return 0, errors.New("Expression Error")
		case 'S':
			if ab != 'A' {
				sf, errs = strconv.ParseFloat(expr[co:i], 64) // 将运算符前的数字取出来
				if errs != nil {
					return 0, errors.New("Expression Error")
				}
				stackA.Push(sf) // 读取的数字进栈
				ab = 'A'
			}
			if ab == 'A' {
				Re := stackA.Pop().(float64)
				if Re >= 0 {
					stackA.Push(math.Sqrt(Re)) // Sqrt(X) X的平方根
					co = uint32(i) + 1         // 移动切片定位标记
					continue
				}
				return 0, errors.New("Expression Error")
			}
			return 0, errors.New("Expression Error")
		case 'P':
			if ab != 'A' { // 标记符前面必须是字符或者为空
				stackA.Push(pi)    // Pi常量进入数字栈
				co = uint32(i) + 1 // 移动切片定位标记
				ab = 'A'
				continue
			}
			return 0, errors.New("Expression Error")
		default:
			return 0, errors.New("Operator Error")
		}
	}
	return 0, errors.New("Possible Error") // 这里应该不会执行，但GO语言要求返回值
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
func priority(x interface{}) uint {
	switch x.(rune) {
	case '+', '-':
		return 1
	case '*', '/', '%':
		return 2 // 返回运算符的优先级等级
	case '^':
		return 3 // 乘方运算符的优先级最高
	default:
		os.Exit(1)
	}
	return 0 // 这里应该不会执行，但GO语言要求返回值
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
func computing(x interface{}) (interface{}, error) {
	switch x.(rune) {
	case '+':
		C1 := stackA.Pop().(float64) // 数字栈栈顶元素赋给C1-C2，并将空接口类型转成浮点类型
		C2 := stackA.Pop().(float64)
		C3 := C2 + C1 // 进行双目运算
		return C3, nil
	case '-':
		C1 := stackA.Pop().(float64) // 数字栈栈顶元素赋给C1-C2，并将空接口类型转成浮点类型
		C2 := stackA.Pop().(float64)
		C3 := C2 - C1 // 进行双目运算
		return C3, nil
	case '*':
		C1 := stackA.Pop().(float64) // 数字栈栈顶元素赋给C1-C2，并将空接口类型转成浮点类型
		C2 := stackA.Pop().(float64)
		C3 := C2 * C1 // 进行双目运算
		return C3, nil
	case '/':
		C1 := stackA.Pop().(float64) // 数字栈栈顶元素赋给C1-C2，并将空接口类型转成浮点类型
		C2 := stackA.Pop().(float64)
		if C1 == 0 {
			return 0, errors.New("Divide By Zero")
		}
		C3 := C2 / C1 // 进行双目运算
		return C3, nil
	case '%':
		C1 := stackA.Pop().(float64) // 数字栈栈顶元素赋给C1-C2，并将空接口类型转成浮点类型
		C2 := stackA.Pop().(float64)
		if C1 == 0 {
			return 0, errors.New("Divide By Zero")
		}
		C3 := math.Mod(C2, C1) // 数学库运算
		return C3, nil
	case '^':
		C1 := stackA.Pop().(float64) // 数字栈栈顶元素赋给C1-C2，并将空接口类型转成浮点类型
		C2 := stackA.Pop().(float64)
		C3 := math.Pow(C2, C1) // 数学库运算
		return C3, nil
	default:
		os.Exit(1)
	}
	return 0, errors.New("Possible Error") // 这里应该不会执行，但GO语言要求返回值
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
